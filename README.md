# rhythmgame

A game that produces notes to play on a computer keyboard. Has 8 columns by default. Score more points for more accurate hits.

This module contains a test visualizer for my FFT algorithm that will be used to generate notes from songs in the future. In order to use it,
ffmpeg must be in script directory, as well as a test song (named "test_song.mp3" by default, can be changed in visualizer.py).
